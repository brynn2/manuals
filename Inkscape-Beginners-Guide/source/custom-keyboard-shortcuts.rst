******************************
Customizing Keyboard Shortcuts
******************************

Inkscape allows you to use your own custom keyboard shortcuts. There are various reasons why you would want to change those:

- you have used a different graphics software before, and would like to keep the
  shortcuts the same, so you don't need to learn so many new ones
- your keyboard layout makes it very hard to press some key combinations
- certain key combinations are already in use on your computer, and you do not want to change them just for Inkscape
- you want to assign a shortcut to a functionality in Inkscape that doesn't have a shortcut by default

You can change the shortcuts at :menuselection:`Edit --> Preferences --> Interface --> Keyboard`.

The dialog lets you choose between some prefilled keyboard shortcut maps that resemble those of other graphics programs.

.. figure:: images/keyboard_shortcuts_choose_keymap.png
   :alt: Choosing a key map
   :class: screenshot

   You can choose between a couple of presets for the keyboard shortcuts.

In addition to this, you can overwrite any shortcut by:

#. selecting it from the list;
#. clicking on the field in the :guilabel:`Shortcut` column;
#. pressing the new shortcut, when it says :guilabel:`New accelerator` in that
   field. Pressing :kbd:`Backspace` will reset the shortcut to its default value.

.. figure:: images/keyboard_shortcuts_new_shortcut.png
   :alt: Setting a new keyboard shortcut
   :class: screenshot

   When it says 'New accelerator', press the new shortcut.
